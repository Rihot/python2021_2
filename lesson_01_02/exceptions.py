"""
Исключения
"""

try:
    a = int(input())

    b = input()
    if b.isdigit():
        b = int(b)
    else:
        print('Вы ввели не число')

    print(a + b)
except ValueError:
    print('Вы ввели не число')
except TypeError as err:
    print(err)
# except:
#     print('Ловит все исключения')
else:
    print('Выполняется, если нет ошибок')
finally:
    print('Выполняется всегда')






