# Однострочный комментарий

'''
Многострочный
комментарий
'''

"""
todo: Как объявить переменную в Python?

PEP-8

todo: Какие типы данных есть в Python?
1. Скалярные (простые): bool int float complex str bytes
2. Структурные (ссылочные): tuple list set dict объекты
3. Специальные: None

result = None

Python - язык с динамической типизацией
a = 1    => int
a = 1.2  => float
a = ''   => str
a = None => NoneType

В Python типы данных делятся еще на две группы:
1. Неизменяемые (immutable): скалярные и tuple
2. Изменяемые (mutable): структрые, кроме tuple
"""

is_student = True
hours = 5
price = 123.40
name = 'Хома'

print(is_student, name)

# todo: bool - Логический тип (логические флаги)
is_debug = True
has_error = False

# todo: int - Целочисленный тип
i1 = 123
i2 = -123
i3 = 0b1011100
i4 = 0o755
i5 = 0xffffff
i6 = 0xAB

# todo: float - Число с плавающей точкой
f1 = 123.45
f2 = 13e6    # 13 * 10 ** 6  => 13000000.0
f3 = 12e-3   # 12 * 10 ** -3 => 0.012

"""
todo: complex - Комплексные числа
z = a + bi
"""
c1 = 1.2+3.4j
c2 = complex(1.2, 3.4) # => 1.2+3.4j
c3 = 3.14j # => 0+3.14j

# todo: str - Строковой тип (по-умолчанию юникод UTF-8)
s1 = '\tHello,\nPython!'
s2 = "Hello, Python!"
s3 = '''
Многострочный
    текст
'''
s4 = """
Многострочный
    текст
"""
print(s3)
print(s1)
s5 = ' "" '
s6 = " '' "
s7 = ' \' \\ '
s8 = ''' "" '' '''
s9 = r'^\d+$' # => raw (регулярные выражения)

# todo: bytes - байтовые строки
b1 = b'Hello, Python!' # only ASCII
b2 = bytes('Привет, Python', 'utf-8')
print(b1)
print(b2)

# todo: tuple - Кортеж

t1 = (1, 1.2, 'Hello', True, (7, 8, 9))
t2 = (789,) # кортеж из одного элемента
print(t1[3], t1[4], t1[4][2])
# t1[3] = False
# del t1[3]

# todo: list - Списки
lst1 = [1, 1.2, 'Hello', True, (7, 8, 9)]
lst1[3] = False
print(lst1)

persons = [
    ('Петя', 35, ('python2', 'php')),
    ('Вася', 25, ('python', 'linux')),
]

# todo: set - множества

set1 = {1, 2, 3, 3, 2, 1}
set1.add(8)
set1.update({5, 6, 8, 2})
print(set1)
set2 = set() # empty set
print(8 in set1)

# todo: dict - Словари
person = {
    'name': 'Вася',
    'age': 25,
    'skills': ('python', 'linux'),
}
print(person['skills'])


# todo: Как определить тип данных переменной в Python?
print(type(person), type(is_student))
print(isinstance(person, dict))
print(isinstance(t1, (tuple, list)))

"""
todo: Как выполнить явное приведение переменой к типу данных?

bool(x)
int(x [, base])
float(x)
complex(real [, imag])
str(x)
tuple(x)
list(x)
set(x)
dict(x)


todo: Какие операторы существует в Python?

арифметические:  + - * / % // **
сравнения:       == != > < >= <=
логические:      and or not
побитовые:       & | ~ ^ << >>
присваивания:    = += -= *= /= %= //= **= &= |= ~= ^= <<= >>=
                 :=
принадлежности:  in
                 not in
тождественности: is
                 is not
                 (использовать только с структурными типами)
"""


# todo: Ветвление

a = 10
b = 10

if a < b:
    # pass
    print('a < b')
elif a == b:
    print('a = b')
else:
    print('a > b')


# todo: Тернарный оператор
# number = input()
# number = int(number) if number.isdigit() else 0
# print(number, type(number))


# todo: Циклы

i = 1

while i:
    if not i % 2:
        print(i)

    if i == 10:
        break # досрочный выход из цикла

    i += 1


for i in range(5):
    print(i)

print('====')

for i in range(1, 11):
    if not i % 2:
        print(i)


for i in range(2, 11, 2):
    print(i)


for index, p in enumerate(persons):
    print(index, p)


for key, value in person.items():
    print(key, value)
