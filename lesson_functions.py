"""
Функции
"""

# todo: Как объявить функцию?
def print_hello():
    print('Hello, Python!')

# todo: Как выполнить вызов функции?
print_hello()
print_hello()
print_hello()

# todo: Как вернуть результат из функции?
def return_hello():
    return 'Hello, Python!'
    # return 1, 2, 3   => возвращаем три значения
    # return (1, 2, 3) => возвращаем одно значение,
    #                     поэтому явно указывает, что это кортеж

result = return_hello()
print(result)


# todo: Как описать и передавать аргументы?
def print_greeting(name):
    print('Hello, ', name, '!', sep='')

print_greeting('Вася')
print_greeting('Петя')

def multi(a, b):
    return a * b

print(multi(1, 2))


# todo: Как задать значения аргументов по-умолчанию?
# def f(a=1, b=2, c=3)
# def f(a, b, c=3, d=4, e=5, f=6)
# f (1, 2, f=10)
def my_pow(x, n=2):
    return x ** n

print(my_pow(5), my_pow(5, 3))

# todo: Позиционные и именованные аргументы
print(my_pow(5, 3))
print(my_pow(5, n=3))
print(my_pow(n=3, x=5))
summa = my_pow(2, 4) + my_pow(2, 5)
print(summa)
# my_pow(n=3, 5) # Exception!


# todo: Переменное количество позиционных аргументов
def super_multi(a, b, *args):
    r = a * b

    for i in args:
        r *= i

    return r


print(super_multi(1, 2)) # => a=1 b=2 args=()
print(super_multi(1, 2, 3))
print(super_multi(1, 2, 3, 4, 5))

# todo: Переменное количество именованных аргументов
def db_connect(provider, **kwargs):
    print('Connect to DB', provider)
    print(kwargs)

db_connect('sqlite', db_name='/tmp/db.sqlite3')
db_connect('mysql', host='localhost', user='root')


def wrapper(*args, **kwargs):
    pass

# todo: Как развернуть кортеж/список
# в значения позиционных аргументов?
lst = [6, 4]
# multi(lst[0], lst[1])
print(multi(*lst), my_pow(*lst))

# todo: Как развернуть словарь
# в значения именованных аргументов?
config = {
    'provider': 'postgres',
    'host': '127.0.0.1',
}
db_connect(**config)
# my_pow(**{'x': 5, 'p': 9}) Error, p не описан в аргументах


# todo:

def split_pieces(s, size, output=None):
    if output is None:
        output = []

    start = 0
    end = len(s)

    while start < end:
        output.append(s[start:start+size])
        start += size

    return output


found = []
split_pieces('Python is programming language!', 4, found)
print(found)


# todo: Анонимная функция
numbers = [1, 2, 3, 4, 5]
squares = list(map(lambda i: i * i, numbers))
print(squares)

odd = list(filter(lambda i: i % 2, numbers))
print(odd)


# todo: Рекурсивная функция
# Прямая рекурсия
def factorial(n):
    """5! = 1 * 2 * 3 * 4 * 5"""
    if n == 0:
        return 1
    return factorial(n - 1) * n
    # return 1 if n == 0 else factorial(n - 1) * n

print('5! =', factorial(5))

"""
Косвенная рекурсия

def f1():
    f2()

def f2():
    f1()

f1()
"""


# todo: Области видимости переменной и время жизни переменной

# print(globals())

g = 1

def f():
    loc = 2

    def child():
        global g
        g += 1

        nonlocal loc
        loc += 1

        print(g)
        print(loc)

    child()

f()














